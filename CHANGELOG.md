<!-- Keep a Changelog guide -> https://keepachangelog.com -->

# Changelog

## 0.0.1
### Added
- First implementation
- Initial scaffold created from [IntelliJ Platform Plugin Template](https://github.com/JetBrains/intellij-platform-plugin-template)
